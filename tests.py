import unittest

from classes.battlegrid import BattleGrid
from classes.segment import Segment
from classes.cell import Cell
from classes.battleship import BattleShip
from classes.segmentsgroup import SegmentsGroup

b = BattleShip(4, 5, 'h', 5)

class test_BattleShip(unittest.TestCase):

    def setUp(self):
        self.b = b


    def test_instantiate(self):        
        self.assertIsInstance(self.b, BattleShip)


    def test_attributes(self):
        self.assertEqual(self.b.health, 5)
        self.assertEqual(self.b.x,4)
        self.assertEqual(self.b.y,5)
        self.assertEqual(self.b._ORIENTATION, 'h')


    def test_segments(self):
        for segment in self.b._segments:
            self.assertIsInstance(segment, Segment)
        
class test_Segment(unittest.TestCase):

    def setUp(self): 
        self.segment = Segment(1, 2, parent=b)


    def test_instantiate(self):
        self.assertIsInstance(self.segment, Segment)


    def test_attributes(self):
        self.assertEqual(self.segment._hit, False)
        self.assertEqual(self.segment.x, 1)
        self.assertEqual(self.segment.y, 2)        
        self.assertTupleEqual(self.segment.coords, (1, 2))

class test_Cell(unittest.TestCase):

    def setUp(self):
        self.cell = Cell(0, 0)

    
    def test_instantiate(self):
        self.assertIsInstance(self.cell, Cell)

    
    def test_attributes(self):
        self.assertEqual(self.cell.x, 0)
        self.assertEqual(self.cell.y, 0)

class test_SegmentsGroup(unittest.TestCase):

    def setUp(self):
        self.b_h = BattleShip(0, 0, 'h', 4)
        self.b_v = BattleShip(0, 0, 'v', 2)
        self.segments_group = SegmentsGroup()


    def test_instantiate(self):
        self.assertIsInstance(self.segments_group, SegmentsGroup)

    
    def test_add_segment(self):
        for segment in self.b_h._segments:
            self.assertNotIn(segment, self.segments_group.segments)
            self.segments_group.add_segment(segment)
            

    def test_add_segment_error(self):
        for segment in self.b_h._segments:
            self.segments_group.add_segment(segment)

        for segment in self.b_v._segments:
            self.segments_group.add_segment(segment)
            self.assertIn(segment, self.segments_group.segments)



if __name__=='__main__':
    unittest.main()