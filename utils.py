def sign(n):
    if n == 0:
        raise ValueError("0 is not positive or negative")
    return int(n > 0) - int(n < 0)
