from .position import Position

class Segment(Position):
    def __init__(self, x, y, index, parent):
        super().__init__(x=x, y=y)
        self._parent = parent
        self._hit = False
        self.index = index

    @property
    def parent(self):
        return self._parent

    @property
    def coords(self) -> tuple:
        """returns tuple of Segment object's coordinates"""
        return (self.x, self.y)

    
    def hit(self):
        self._hit = True


    def is_hit(self):
        return self._hit


    def display(self):
        if self.is_hit():
            print(' X', end='')
        else:
            print(' \u2610', end='')

    def __repr__(self):
        return f"<SEGMENT {self.x},{self.y}>"

