import abc

class Position(abc.ABC):
    # @abc.abstractclassmethod    
    def __init__(self, x=0, y=0):
        self._x = x
        self._y = y        
    
    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y