from .cell import Cell
from .battleship import BattleShip

class BattleGrid:
    def __init__(self, sg=None):
        self._grid = self._gen_grid()
        self.sg = sg

    def _gen_grid(self):
        return [[Cell(y, x) for x in range(10)] for y in range(10)]

    @property
    def grid(self):
        return self._grid

    
    def grid_at(self, x, y, value):
        self.grid[y][x].contents = value


    def check_grid(self, x:int, y:int, o:str, l:int) -> bool:
        """Returns True if cells avaible to place battleship object"""
        for i in range(l):
            if o in ['h', 'horizontal']:
                if self.grid[y][x + i].contents is not None:
                    return False
            elif o in ['v', 'vertical']:
                if self.grid[y + i][x].contents is not None:
                    return False            
        return True


    def place_battleship(self, b:BattleShip) -> bool:
        """sets Cells in grid to reference Segment objects of Battleship"""
        if self.check_grid(x=b.x, y=b.y, o=b._ORIENTATION, l=b._LENGTH):
            for segment in b.segments:
                self.grid_at(segment.x, segment.y, segment)
                self.sg.add_segment(segment)
            return True
        return False
        

    def display(self):
        for row in self.grid:
            for cell in row:
                cell.display()
            print()


if __name__=='__main__':
    g = BattleGrid()
    b = BattleShip(0, 0, 'h', 5)

    g.place_battleship(b)
