from classes.battlegrid import BattleGrid
from classes.battleship import BattleShip
from classes.segmentsgroup import SegmentsGroup

class Engine:
    def __init__(self):
        self.sg = SegmentsGroup()
        self.g = BattleGrid(self.sg)        
        self.b = BattleShip(0, 0, 'h', 5)
        self.b2 = BattleShip(5, 5, 'v', 2)

    
    def place_ships(self):
        self.g.place_battleship(self.b)
        self.g.place_battleship(self.b2)

    
    def attack(self, x, y):
        if not self.g.grid[y][x].is_attacked():
            self.g.grid[y][x]._attacked = True
    
    
    def display(self):
        self.g.display()

    