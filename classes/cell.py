from .position import Position

class Cell(Position):
    def __init__(self, x, y):
        super().__init__(x=x, y=y)
        self._contents = None
        self._attacked = False

    @property
    def contents(self):
        return self._contents
    
    @contents.setter
    def contents(self, item):
        self._contents = item


    def is_attacked(self):
        return self._attacked


    def display(self):
        if self.contents:
            return self.contents.display()
        elif  self.is_attacked():
            print(" O", end='')
        else:
            print(" .", end='')

    def __repr__(self):
        return f"<CELL {self.x},{self.y}>"