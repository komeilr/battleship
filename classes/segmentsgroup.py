from .segment import Segment

class SegmentsGroup:
    segments = []

    
    def add_segment(self, segment:Segment) -> bool:
        if segment in SegmentsGroup.segments:
            raise ValueError('segment already in group')
        SegmentsGroup.segments.append(segment)