from classes.position import Position
from classes.segment import Segment


class BattleShip(Position):
    """Object representing a Battleship"""
    def __init__(self, x, y, orientation=None, length=None):
        super().__init__(x=x, y=y)
        self._ORIENTATION = self._validate_orientation(orientation)
        self._LENGTH = self._clamp_length(length, 2, 5)
        self._segments = self._generate_segments()                
        self._health = self._get_health()

    
    def _validate_orientation(self, o:str) -> str:
        """Ensures orientation is one of four values"""
        if o.lower() not in ['h', 'v', 'horizontal', 'vertical']:
            raise ValueError('invalid orientation')
        return o.lower()


    def _clamp_length(self, l:int, low:int, high:int) -> int:
        """sets a minimum and maximum value on length inclusive(2, 5)"""
        if l is None:
            raise AttributeError("length required")
        for n in [l, low, high]:
            if not isinstance(n, int):
                raise ValueError("value must be integer")

        return min(max(low, l), high)

    
    def _generate_segments(self) -> list:
        """returns a list of Segment objects representing a Battleship segment on the grid"""
        return [
            Segment(self.x + i, self.y, index = i, parent=self) \
            if self._ORIENTATION in ['h', 'horizontal'] \
            else Segment(self.x, self.y + i, index = i, parent=self) \
                for i in range(self._LENGTH)
                ]

    def _get_health(self) -> int:
        """returns the health of the BattleShip object"""
        return sum([not int(s._hit) for s in self._segments])


    @property
    def health(self):
        return self._health

    @property
    def segments(self):
        return self._segments

    def __repr__(self):
        return f"<BATTLESHIP {self.health}/{self._LENGTH}>"

if __name__=='__main__':
    b = BattleShip(0, 0, 'horizontal', 5)
    print(b.x, b.y)
    print(b._LENGTH)
    print(b._ORIENTATION)
    for seg in b._segments:
        print(seg)